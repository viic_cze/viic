package cz.viic.android.customer.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.customer.R;

public class ConfirmationActivity extends ActionBarActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmation);
		
		ActionBar ab = getSupportActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
		ab.setTitle("Hotovo");
		
		Ui.textView(this, R.id.message).setText(IntentTool.geMessage(getIntent()));
	}
	
	public static class IntentTool {
		public static Intent create(Context c, String message) {
			Intent i = new Intent(c, ConfirmationActivity.class);
			i.putExtra("message", message);
			return i;
		}

		private static String geMessage(Intent intent) {
			return (String) intent.getStringExtra("message");
		}
	}
}

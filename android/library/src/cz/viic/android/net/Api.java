package cz.viic.android.net;

import java.lang.reflect.Type;
import java.util.List;

import com.android.volley.Request;
import com.google.gson.reflect.TypeToken;

import cz.viic.android.app.App;
import cz.viic.android.model.CustomerAppData;
import cz.viic.android.model.Entity;
import cz.viic.android.model.MerchantAppData;
import cz.viic.android.model.Program;
import cz.viic.android.model.Subscription;
import cz.viic.android.model.User;

public class Api {
	public static void getPrograms(NetworkListener<List<Program>> listener, String merchantId,
			boolean includeSubscriptions) {
		String urlSuffix = "/programs?";
		urlSuffix += "merchant_id=" + merchantId;
		if (includeSubscriptions) urlSuffix += "&fields=subscriptions";

		Type type = new TypeToken<List<Program>>() {
		}.getType();
		Request<List<Program>> request = new ApiGetRequest<List<Program>>(urlSuffix, listener,
				type);
		App.getVolleyQueue().add(request);
	}

	public static Request<?> getMerchantAppData(String merchantId,
			NetworkListener<MerchantAppData> listener) {
		String urlSuffix = "/merchant_app_data?merchant_id=" + merchantId;
		Type type = MerchantAppData.class;
		Request<?> request = new ApiGetRequest<MerchantAppData>(urlSuffix, listener, type);
		App.getVolleyQueue().add(request);
		return request;
	}

	public static Request<?> getCustomerAppData(String userId,
			NetworkListener<CustomerAppData> listener) {
		String urlSuffix = "/customer_app_data?user_id=" + userId;
		Type type = CustomerAppData.class;
		Request<?> request = new ApiGetRequest<CustomerAppData>(urlSuffix, listener, type);
		App.getVolleyQueue().add(request);
		return request;
	}

	public static void putSubscription(Subscription subscription,
			NetworkListener<Object> listener) {
		Request<Object> request = new ApiPutRequest("subscriptions", subscription, listener);
		App.getVolleyQueue().add(request);
	}

	public static Request<?> put(String collection, Entity entity,
			NetworkListener<Object> listener) {
		Request<?> request = new ApiPutRequest(collection, entity, listener);
		App.getVolleyQueue().add(request);
		return request;
	}
}

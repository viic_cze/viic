package cz.fhucho.workers;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Utils {
	public static ThreadPoolExecutor createDefaultWorkersExecutor() {
		// Max 1 thread, will die after 1 seconds of being idle.
		ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
					@Override
					public Thread newThread(Runnable r) {
						Thread t = Executors.defaultThreadFactory().newThread(r);
						t.setPriority(Thread.MIN_PRIORITY);
						return t;
					}
				});
		executor.allowCoreThreadTimeOut(true);
		return executor;
	}
}

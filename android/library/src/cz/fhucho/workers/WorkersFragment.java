package cz.fhucho.workers;

import java.util.HashSet;
import java.util.Set;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class WorkersFragment extends Fragment {
	public static final String TAG = "WorkersFragment";

	private Set<Worker<?, ?>> workers = new HashSet<Worker<?, ?>>();

	public void addWorker(Worker<?, ?> worker) {
		workers.add(worker);
	}

	public <T extends Worker<?, P>, P> T findWorker(Class<T> cls, P params) {
		for (Worker<?, ?> w : workers) {
			if (w.getClass().equals(cls) && w.getParams().equals(params)) {
				return cls.cast(w);
			}
		}
		return null;
	}
	
	public boolean removeWorker(Worker<?, ?> worker) {
		return workers.remove(worker);
	}

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setRetainInstance(true);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		for (Worker<?, ?> w : workers) {
			w.removeAllListeners();
		}
	}
}

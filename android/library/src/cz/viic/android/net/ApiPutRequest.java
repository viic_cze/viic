package cz.viic.android.net;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cz.viic.android.model.Entity;

public class ApiPutRequest extends ApiRequest<Object> {
	private static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(
			FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

	private final Entity entity;

	private byte[] body;

	public ApiPutRequest(String collection, Entity entity, NetworkListener<Object> listener) {
		super(Method.PUT, URL_PREFIX + "/" + collection + "/" + entity.getId(), listener);
		this.entity = entity;
		setShouldCache(false);
	}

	@Override
	public byte[] getBody() throws AuthFailureError {
		if (body == null) body = GSON.toJson(entity).getBytes();
		return body;
	}

	@Override
	protected Response<Object> parseNetworkResponse(NetworkResponse response) {
		return Response.success(new Object(), null);
	}
}

package cz.fhucho.simple_disk_cache;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import org.apache.commons.io.IOUtils;

public class Entries {
	private final File file;
	private final Map<Long, Entries.Entry> entries = new HashMap<Long, Entries.Entry>();
	private final PriorityQueue<Long> holes = new PriorityQueue<Long>();

	public Entries(File file) {
		this.file = file;
	}

	public void initialize() throws IOException {
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new FileInputStream(file));
		} finally {
			IOUtils.closeQuietly(dis);
		}
	}

	public Object get(long hashedKey) {
		return null;
	}
	
	public void put(long hashedKey, long size, long lastAccessTime) {

	}
	
	public long totalSize() {
		return 0;
	}

	private Entries.Entry readEntry(RandomAccessFile file) throws IOException {
		boolean valid = file.readBoolean();
		if (!valid) return null;

		long offset = file.getFilePointer();
		long hashedKey = file.readLong();
		long size = file.readLong();
		long lastAccessTime = file.readLong();

		return new Entry(offset, hashedKey, size, lastAccessTime);
	}

	private static class Entry {
		final long offsetInEntriesFile;
		final long hashedKey;
		final long size;
		final long lastAccessTime;

		public Entry(long offsetInEntriesFile, long hashedKey, long size, long lastAccessTime) {
			this.offsetInEntriesFile = offsetInEntriesFile;
			this.hashedKey = hashedKey;
			this.size = size;
			this.lastAccessTime = lastAccessTime;
		}
	}
}
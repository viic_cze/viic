package cz.fhucho.w;

import android.support.v4.app.FragmentActivity;

public abstract class Worker {

	private FragmentActivity activity;
	private WorkersFragment fragment;

	protected void onCreate() {
	}

	protected void onAttached() {
	}

	protected void onDetached() {
	}

	protected void onDestroy() {
	}

	protected FragmentActivity getActivity() {
		return activity;
	}

	void attachActivity(FragmentActivity activity) {
		this.activity = activity;
	}

	void detachActivity() {
		this.activity = null;
	}

	public void start(FragmentActivity activity) {
		fragment = WorkersFragment.getOrCreate(activity);
		fragment.addWorker(this);

		onCreate();
		attachActivity(activity);
		onAttached();
	}

	public void destroy() {
		fragment.removeWorker(this);
		onDestroy();
	}

}

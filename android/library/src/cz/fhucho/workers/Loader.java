package cz.fhucho.workers;

import java.util.concurrent.Executor;

import cz.fhucho.workers.Loader.Listener;

public abstract class Loader<T, P> extends Worker<Listener<T>, P> {
	private static Executor executor = Utils.createDefaultWorkersExecutor();
	
	private T result;

	public abstract T onLoad();
	
	@Override
	public void onCancel() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<Listener<T>> getTypeOfListener() {
		return (Class<Listener<T>>) new Listener<T>() {
			@Override
			public void onLoaded(T result) {
			}
		}.getClass().getInterfaces()[0];
	}

	public void load() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				if(result == null) result = onLoad();
				getListenersProxy().onLoaded(result);
			}
		});
	}

	public interface Listener<T> {
		public void onLoaded(T result);
	}

}

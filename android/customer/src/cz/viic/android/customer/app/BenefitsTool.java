package cz.viic.android.customer.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.customer.R;
import cz.viic.android.model.Merchant;
import cz.viic.android.model.Program;
import cz.viic.android.model.UserMerchantAssoc;

public class BenefitsTool {
	private static void addBenefit(ViewGroup container, String name, String note,
			boolean highlighted) {
		LayoutInflater inflater = LayoutInflater.from(container.getContext());
		View benefit = inflater.inflate(R.layout.item_benefit, container, false);
		container.addView(benefit);

		TextView nameTextView = Ui.view(benefit, R.id.name);
		nameTextView.setText(name.toUpperCase());
		if (!highlighted) {
			nameTextView.setBackgroundColor(0xFFE4E4E4);
			nameTextView.setTextColor(0xFF111111);
		}

		Ui.textView(benefit, R.id.note).setText(note);
	}

	public static void setUpBenefits(ViewGroup container, Merchant merchant, boolean expanded) {
		container.removeAllViews();

		UserMerchantAssoc assoc = merchant.getUserMerchantAssoc();
		Program program = merchant.getProgram();

		if (assoc == null) {
			if (program.getEntryBonusTitle() != null) {
				addBenefit(container, program.getEntryBonusTitle(), "vstupní bonus", false);
			}

			if (expanded) {
				String note = "za " + program.getRequiredPoints() + " " + program.getUnit();
				addBenefit(container, program.getTitle(), note, false);
			}
		} else {
			if (assoc.isEntryBonusAvailable() && program.getEntryBonusTitle() != null) {
				String title = program.getEntryBonusTitle();
				addBenefit(container, title, "můžete si vyzvednout", true);
			}

			for (int i = 0; i < assoc.getPoints() / program.getRequiredPoints(); i++) {
				addBenefit(container, program.getTitle(), "můžete si vyzvednout", true);
			}

			int progress = assoc.getPoints() % program.getRequiredPoints();
			String note = progress + " / " + program.getRequiredPoints() + " "
					+ program.getUnit();
			addBenefit(container, program.getTitle(), note, false);
		}
	}
}

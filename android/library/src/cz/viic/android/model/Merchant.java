package cz.viic.android.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Merchant extends Entity implements Serializable {
	private List<Program> programs;
	private List<UserMerchantAssoc> userMerchantAssocs;
	private String name;

	public String getName() {
		return name;
	}
	
	public Program getProgram() {
		if (programs.size() > 0) {
			return programs.get(0);
		} else {
			return null;
		}
	}

	public List<Program> getPrograms() {
		return programs;
	}
	
	public UserMerchantAssoc getUserMerchantAssoc() {
		if (userMerchantAssocs.size() > 0) {
			return userMerchantAssocs.get(0);
		} else {
			return null;
		}
	}

	public List<UserMerchantAssoc> getUserMerchantAssocs() {
		return userMerchantAssocs;
	}
}

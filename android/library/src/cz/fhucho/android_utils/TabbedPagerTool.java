package cz.fhucho.android_utils;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;

public class TabbedPagerTool {
	
	public static void init(ActionBarActivity activity, List<TabDescriptor> tabDescriptors, int pagerId) {
		TabbedPagerTool t = new TabbedPagerTool(activity, tabDescriptors, pagerId);
		activity.getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		t.initPager();
		t.initTabs();
	}
	
	//–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
	
	private final ActionBarActivity activity;
	private final FragmentManager fragmentManager;
	private final List<TabDescriptor> tabDescriptors;
	private final ViewPager pager;

	private TabbedPagerTool(ActionBarActivity activity, List<TabDescriptor> tabDescriptors, int pagerId) {
		this.activity = activity;
		this.tabDescriptors = tabDescriptors;
		this.pager = Ui.view(activity, pagerId);
		this.fragmentManager = activity.getSupportFragmentManager();
	}

	private void initPager() {
		pager.setOffscreenPageLimit(Integer.MAX_VALUE);
		pager.setAdapter(new TabsPagerAdapter());
		pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				activity.getSupportActionBar().setSelectedNavigationItem(position);
			}
		});
	}

	private void initTabs() {
		SimpleTabListener tabListener = new SimpleTabListener() {
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				pager.setCurrentItem(tab.getPosition());
			}
		};

		ActionBar ab = activity.getSupportActionBar();
		for (TabDescriptor td : tabDescriptors) {
			ab.addTab(ab.newTab().setTabListener(tabListener).setText(td.title));
		}
	}

	private class TabsPagerAdapter extends FragmentPagerAdapter {
		public TabsPagerAdapter() {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int i) {
			return tabDescriptors.get(i).fragment;
		}

		@Override
		public int getCount() {
			return tabDescriptors.size();
		}
	}

	public static class TabDescriptor {
		private final Fragment fragment;
		private final String title;

		public TabDescriptor(Fragment fragment, String title) {
			this.fragment = fragment;
			this.title = title;
		}
	}
}
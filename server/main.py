#!/usr/bin/env python

import json
import logging
import endpoints

from db import *
from flask import *

app = Flask(__name__)

@app.route("/", methods=['GET'])
def index():
	return "Welcome to the Viic API!"


@app.route("/subscriptions/<id>", methods=['PUT'])
def put_subscription(id):
	item = request.get_json(force=True)
	insert_or_replace('subscriptions', item)
	return Response()


@app.after_request
def after_request(response):
	print('–' * 80)
	print(request.method + ' ' + request.url + ' | ' + request.remote_addr)
	if len(request.data) > 0:
		print(request.data.decode("utf-8"))
	print()
	
	print(response.status)
	print(response.data.decode("utf-8"))
	
	return response


@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database', None)
	if db is not None: db.close()


log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app.add_url_rule('/customer_app_data', view_func=endpoints.get_customer_app_data)
app.add_url_rule('/merchant_app_data', view_func=endpoints.get_merchant_app_data)
app.add_url_rule('/user_merchant_assocs/<id>', view_func=endpoints.put_user_merchant_assoc, methods=['PUT'])
app.add_url_rule('/<collection>/<id>', view_func=endpoints.put, methods=['PUT'])

app.run(host='0.0.0.0', port=80, debug=True)

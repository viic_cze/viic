package cz.viic.android.merchant;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.model.MerchantAppData;
import cz.viic.android.model.Stats;

public class StatsFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.frag_stats, container, false);
	}
	
	public void onDataLoaded(MerchantAppData data) {
		System.out.println(getView() == null);
		Stats stats = data.getStats();
		Ui.textView(getView(), R.id.paid_out).setText(String.valueOf(stats.getPaidOut()) + " Kč");
		Ui.textView(getView(), R.id.customers_new).setText(String.valueOf(stats.getCustomersNew()));
		Ui.textView(getView(), R.id.customers_served).setText(String.valueOf(stats.getCustomersServed()));
		Ui.textView(getView(), R.id.customers_total).setText(String.valueOf(stats.getCustomersTotal()));
	}
}

package cz.viic.android.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Subscription extends Entity implements Serializable {
	private int points;
	private transient Program program;
	private String username;
	private String programId;

	public Subscription() {
	}

	public Subscription(String id, String username, String programId, int points) {
		super(id);
		this.username = username;
		this.programId = programId;
		this.points = points;
	}

	public Program getProgram() {
		return program;
	}
	
	public void setProgram(Program program) {
		this.program = program;
	}

	public int getPoints() {
		return points;
	}
	
	public void setPoints(int points) {
		this.points = points;
	}

	public String getUsername() {
		return username;
	}

	public String getProgramId() {
		return programId;
	}
}

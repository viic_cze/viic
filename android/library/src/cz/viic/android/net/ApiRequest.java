package cz.viic.android.net;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class ApiRequest<T> extends Request<T> {
	protected static final String URL_PREFIX = "http://ec2-54-229-98-246.eu-west-1.compute.amazonaws.com";

	protected static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(
			FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

	private final NetworkListener<T> listener;

	public ApiRequest(int method, String url, NetworkListener<T> listener) {
		super(method, url, null);
		this.listener = listener;
	}

	@Override
	public void deliverError(VolleyError error) {
		listener.onError(error);
	}

	@Override
	protected void deliverResponse(T response) {
		listener.onSuccess(response);
	}

//	@Override
//	public Map<String, String> getHeaders() throws AuthFailureError {
//		String login = Settings.get().getUsername() + ":" + Settings.get().getPassword();
//		String loginBase64 = Base64.encodeToString(login.getBytes(), Base64.DEFAULT);
//
//		Map<String, String> headers = new HashMap<String, String>(1);
//		headers.put("Authorization", "Basic " + loginBase64);
//		return headers;
//	}

}

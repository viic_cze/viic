package cz.viic.android.merchant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.model.MerchantAppData;
import cz.viic.android.model.Program;
import cz.viic.android.model.UserMerchantAssoc;
import cz.viic.android.utils.QrScannerView;

public class ScannerFragment extends Fragment implements QrScannerView.OnQrScannedListener {
	private static final int REQUEST_USER = 0;

	private MerchantAppData merchantAppData;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_USER && resultCode == Activity.RESULT_OK) {
			((MainActivity) getActivity()).loadData();
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_scanner, container, false);
		QrScannerView qrScanner = Ui.view(view, R.id.camera_preview);
		qrScanner.setOnQrScannedListener(this);
		return view;
	}

	public void onDataLoaded(MerchantAppData data) {
		merchantAppData = data;
	}

	@Override
	public void onQrScanned(String userId) {
		UserMerchantAssoc assoc = null;
		for (UserMerchantAssoc a : merchantAppData.getUserMerchantAssocs()) {
			if (userId.equals(a.getUserId())) {
				assoc = a;
				break;
			}
		}

		if (assoc == null) {
			assoc = new UserMerchantAssoc(userId, "d88ce7243b4a358ccf75881e19ffa84a");
		}
		Program program = merchantAppData.getPrograms().get(0);
		Intent i = UserActivity.IntentTool.create(getActivity(), assoc, program);
		startActivityForResult(i, REQUEST_USER);
	}

	public void removeQrScannerView() {
		((ViewGroup) getView()).removeAllViews();
	}
}

package cz.viic.android.app;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class ProgressDialogFragment extends DialogFragment {
	private static final String DEFAULT_TAG = "progress-dialog";

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage(getArguments().getString("message"));
		return dialog;
	}

	public static void dismiss(FragmentActivity activity) {
		FragmentManager fm = activity.getSupportFragmentManager();
		((ProgressDialogFragment) fm.findFragmentByTag(DEFAULT_TAG)).dismiss();
	}

	public static void show(String message, FragmentActivity activity) {
		FragmentManager fm = activity.getSupportFragmentManager();
		
		Bundle args = new Bundle();
		args.putString("message", message);

		DialogFragment d = new ProgressDialogFragment();
		d.setArguments(args);
		d.show(fm, DEFAULT_TAG);
	}
}
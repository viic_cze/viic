package cz.fhucho.android_utils;

public class Measure {
	private static long t;
	
	public static void start() {
		t = System.nanoTime();
	}
	
	public static void stop() {
		System.out.println((System.nanoTime() - t) / 1000000.0 + " ms");
	}
}

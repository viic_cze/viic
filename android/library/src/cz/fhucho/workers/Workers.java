package cz.fhucho.workers;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class Workers {
	public static <W extends Worker<?, ?>> void cancel(W worker, FragmentActivity activity) {
		boolean removed = getOrCreateFragment(activity).removeWorker(worker);
		if(!removed) throw new RuntimeException("Worker isn't in the list of workers.");
		worker.onCancel();
	}
	
	public static <T extends Worker<?, P>, P> T get(Class<T> cls, P params, FragmentActivity activity) {
		return getOrCreateFragment(activity).findWorker(cls, params);
	}

	public static <T extends Worker<?, P>, P> T create(Class<T> cls, P params, FragmentActivity activity) {
		T worker = instantiteWorker(cls, params, new Handler());
		getOrCreateFragment(activity).addWorker(worker);
		return worker;
	}

	public static <T extends Loader<U, P>, U, P> T load(Class<T> cls, P params,
			Loader.Listener<U> listener, FragmentActivity activity) {
		T loader = get(cls, params, activity);
		if (loader == null) loader = create(cls, params, activity);
		loader.addListener(listener);
		loader.load();
		return loader;
	}

	private static WorkersFragment getOrCreateFragment(FragmentActivity activity) {
		FragmentManager fm = activity.getSupportFragmentManager();
		WorkersFragment frag = (WorkersFragment) fm.findFragmentByTag(WorkersFragment.TAG);
		if (frag == null) {
			frag = new WorkersFragment();
			fm.beginTransaction().add(frag, WorkersFragment.TAG).commitAllowingStateLoss();
			fm.executePendingTransactions();
		}

		return frag;
	}

	private static <T extends Worker<?, P>, P> T instantiteWorker(Class<T> cls, P params, Handler handler) {
		try {
			T worker = cls.newInstance();
			worker.init(params, handler);
			return worker;
		} catch (InstantiationException e) {
			throw new RuntimeException("Worker's constructor couldn't be instantiated.", e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("The class " + cls.getSimpleName()
					+ " must be public and must have an empty public constructor.", e);
		}
	}
}

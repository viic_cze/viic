package cz.viic.android.customer.app;

import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragment extends SupportMapFragment {
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		String merchantName = getArguments().getString("merchantName");
		
		LatLng prague = new LatLng(50.083, 14.466);

		GoogleMap map = getMap();
		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(prague, 15));

		map.addMarker(new MarkerOptions().title(merchantName).position(prague));
	}
}

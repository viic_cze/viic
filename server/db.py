import sqlite3
from flask import g

DB_FILENAME = 'database.db'

def dict_from_row(cursor, row):
	d = {}
	for idx, value in enumerate(row):
		d[cursor.description[idx][0]] = value
	return d


def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DB_FILENAME)
		db.row_factory = dict_from_row
	return db


def insert_or_replace(table, values):
	keys = values.keys()
	columns = ', '.join(keys)
	placeholders = ', '.join([':' + k for k in keys])
	sql = 'INSERT OR REPLACE INTO {} ({}) VALUES ({})'.format(table, columns, placeholders)
	db = get_db()
	cur = db.execute(sql, values)
	db.commit()


def query(query, *args):
	cur = get_db().execute(query, args)
	rows = cur.fetchall()
	cur.close()
	return rows

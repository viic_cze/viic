package cz.viic.android.model;

import java.util.List;

public class CustomerAppData {
	private List<Merchant> merchants;
	
	public List<Merchant> getMerchants() {
		return merchants;
	}
}

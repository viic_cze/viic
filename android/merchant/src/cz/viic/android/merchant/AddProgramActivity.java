package cz.viic.android.merchant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.model.Program;

public class AddProgramActivity extends ActionBarActivity {
	private Program getProgram() {
		String title = Ui.textView(this, R.id.title).getText().toString();
		int requiredPoints = Integer.parseInt(Ui.textView(this, R.id.required_points)
				.getText().toString());
		String unit = Ui.textView(this, R.id.unit).getText().toString();
		String entryBonusTitle = Ui.textView(this, R.id.entry_bonus_title).getText()
				.toString();
		int cost = Integer.parseInt(Ui.textView(this, R.id.cost).getText().toString());
		int entryBonusCost = Integer.parseInt(Ui.textView(this, R.id.entry_bonus_cost)
				.getText().toString());

		return new Program("d88ce7243b4a358ccf75881e19ffa84a", title, requiredPoints, unit,
				cost, entryBonusTitle, entryBonusCost);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_program);
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
		getSupportActionBar().setTitle("Vytvořit program");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_program, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.done) {
			setResult(RESULT_OK, ResultTool.create(getProgram()));
			finish();
		}
		return true;
	}

	public static class ResultTool {
		public static Intent create(Program program) {
			Intent intent = new Intent();
			intent.putExtra("program", program);
			return intent;
		}

		public static Program getProgram(Intent result) {
			return (Program) result.getSerializableExtra("program");
		}
	}
}

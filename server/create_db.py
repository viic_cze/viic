#!/usr/bin/env python

import os
import random
import sqlite3

def new_id():
	return '{:0>32x}'.format(random.getrandbits(128))

def insert(table, values):
	keys = values.keys()
	columns = ', '.join(keys)
	placeholders = ', '.join([':' + k for k in keys])
	sql = 'INSERT INTO {} ({}) VALUES ({})'.format(table, columns, placeholders)
	con.execute(sql, values)

DB_FILENAME = 'database.db'

try:
	os.remove(DB_FILENAME)
except FileNotFoundError:
	pass
con = sqlite3.connect(DB_FILENAME)

sql_script = open('schema.sql', 'r').read()
con.executescript(sql_script)

con.commit()
con.close()

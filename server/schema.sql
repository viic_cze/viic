CREATE TABLE merchants(
	id TEXT PRIMARY KEY NOT NULL,
	name TEXT NOT NULL
);
	
CREATE TABLE user_merchant_assocs(
	id TEXT PRIMARY KEY NOT NULL,
	created INTEGER NOT NULL,
	user_id TEXT NOT NULL,
	merchant_id TEXT NOT NULL,
	last_paid_out INTEGER NOT NULL,
	points INTEGER NOT NULL,
	entry_bonus_available INTEGER NOT NULL
);
	
CREATE TABLE programs(
	id TEXT PRIMARY KEY NOT NULL,
	merchant_id TEXT NOT NULL,
	title TEXT NOT NULL,
	required_points INTEGER NOT NULL,
	unit TEXT NOT NULL,
	cost INTEGER NOT NULL,
	entry_bonus_title TEXT NOT NULL,
	entry_bonus_cost INTEGER NOT NULL
);

CREATE TABLE transactions(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	merchant_id TEXT NOT NULL,
	paid_out INTEGER NOT NULL,
	time INTEGER NOT NULL,
	user_id TEXT NOT NULL
);
	
CREATE TABLE users(
	id TEXT PRIMARY KEY NOT NULL,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
);
	
CREATE TABLE reg_ids(
	id TEXT PRIMARY KEY NOT NULL,
	user_id TEXT NOT NULL,
	reg_id TEXT UNIQUE NOT NULL
);

INSERT INTO merchants
VALUES ('d88ce7243b4a358ccf75881e19ffa84a', 'Starbucks');

INSERT INTO merchants
VALUES ('73d51e4292bf14965a822c6b8026be66', 'Potrefená Husa');

INSERT INTO merchants
VALUES ('085ba4853e2807c0e25ea2a551036242', 'Burger King');

INSERT INTO merchants
VALUES ('2ed9aaf119325ce5afefd68de1064fc6', 'SUBWAY');

INSERT INTO programs
VALUES ('e1e5354f516cf0e5ac03acbb6966a71f', '73d51e4292bf14965a822c6b8026be66', 'Polední menu zdarma', 5, 'obědů', 70, 'Sleva 5% na cokoliv', 20);

INSERT INTO programs
VALUES ('379b021dd867a0789034cfb876fe338b', '085ba4853e2807c0e25ea2a551036242', 'Nápoj zdarma', 20, 'utracených Kč', 20, 'Pivo zdarma', 20);

INSERT INTO programs
VALUES ('f7ffcbb7d8af016063ce0a55db75d59c', '2ed9aaf119325ce5afefd68de1064fc6', 'Poloviční sendvič zdarma', 10, 'sendvičů', 50, 'Káva zdarma', 20);

INSERT INTO users
VALUES ('7c6d1b2813f3b549742f95b60ba1ba53', 'fhucho', 'franta');

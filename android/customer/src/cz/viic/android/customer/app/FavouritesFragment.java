package cz.viic.android.customer.app;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.viic.android.model.CustomerAppData;
import cz.viic.android.model.Merchant;

public class FavouritesFragment extends MerchantListFragment {
	@Override
	public void onDataLoaded(CustomerAppData data) {
		List<Merchant> merchants = new ArrayList<Merchant>(data.getMerchants());
		for (Iterator<Merchant> i = merchants.iterator(); i.hasNext();) {
			if (i.next().getUserMerchantAssoc() == null) i.remove();
		}
		setMerchants(merchants);
	}
}

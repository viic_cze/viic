package cz.fhucho.workers;

import java.util.ArrayList;
import java.util.List;

import android.os.Handler;

public abstract class Worker<T, P> {
	private boolean initCalled = false;
	private List<T> listeners;
	private T listenersProxy;
	private P params;

	public abstract void onCancel();
	public abstract Class<T> getTypeOfListener();

	void addListener(T listener) {
		listeners.add(listener);
	}

	void init(P params, Handler handler) {
		if (initCalled) throw new IllegalStateException("init() called before");
		initCalled = true;

		this.params = params;
		listeners = new ArrayList<T>();
		listenersProxy = HandlerProxy.createProxy(listeners, getTypeOfListener(), handler);
	}

	protected P getParams() {
		return params;
	}

	protected T getListenersProxy() {
		return listenersProxy;
	}

	void removeAllListeners() {
		listeners.clear();
	}
	
}

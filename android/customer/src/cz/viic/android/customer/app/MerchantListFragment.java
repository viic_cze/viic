package cz.viic.android.customer.app;

import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import cz.fhucho.android_utils.BetterArrayAdapter;
import cz.fhucho.android_utils.Measure;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.customer.R;
import cz.viic.android.model.CustomerAppData;
import cz.viic.android.model.Merchant;

public class MerchantListFragment extends Fragment {
	private static final int REQUEST_MERCHANT = 0;

	private MerchantsAdapter merchantsAdapter;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_MERCHANT) {
			((MainActivity) getActivity()).loadData();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		merchantsAdapter = new MerchantsAdapter();

		ListView listView = (ListView) inflater
				.inflate(R.layout.frag_nearby, container, false);
		listView.addHeaderView(new View(getActivity()));
		listView.addFooterView(new View(getActivity()));
		listView.setAdapter(merchantsAdapter);

		return listView;
	}

	public void onDataLoaded(CustomerAppData data) {
		setMerchants(data.getMerchants());
	}

	protected void setMerchants(List<Merchant> merchants) {
		merchantsAdapter.setItems(merchants);
	}

	private class MerchantsAdapter extends BetterArrayAdapter<Merchant> {

		public MerchantsAdapter() {
			super(getActivity(), R.layout.item_merchant);
		}

		@Override
		protected void setUpView(int position, final Merchant merchant, View view) {
			Ui.textView(view, R.id.name).setText(merchant.getName());

			ViewGroup container = Ui.view(view, R.id.benefits);
			BenefitsTool.setUpBenefits(container, merchant, false);

			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = MerchantActivity.IntentTool.create(getActivity(), merchant);
					startActivityForResult(i, REQUEST_MERCHANT);
				}
			});
		}
	}

	public static class SubscribingDialog extends DialogFragment {
		public static final String TAG = "SubscribingDialog";

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Připojuji k programu");
			return dialog;
		}

		public static void createAndShow(FragmentManager fm) {
			new SubscribingDialog().show(fm, TAG);
		}

		public static void dismiss(FragmentManager fm) {
			((SubscribingDialog) fm.findFragmentByTag(TAG)).dismiss();
		}
	}

	public static class LoginRequiredDialog extends DialogFragment {

		public void login(View view) {
			dismiss();
			// Intent intent = new Intent(getActivity(), LoginActivity.class);
			// startActivityForResult(intent, REQUEST_LOG_IN);
		}

		public void register(View view) {
			dismiss();
			startActivity(new Intent(getActivity(), RegisterActivity.class));
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View view = inflater.inflate(R.layout.dialog_login_required, null);

			Measure.start();
			Ui.bindClick(view, R.id.register, this, "register");
			Ui.bindClick(view, R.id.login, this, "login");
			Measure.stop();

			Dialog dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(view);
			return dialog;
		}
	}
}

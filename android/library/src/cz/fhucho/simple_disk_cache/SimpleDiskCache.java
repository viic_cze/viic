package cz.fhucho.simple_disk_cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class SimpleDiskCache {
//	private static final double TRIM_RATIO = 0.9;
//	private static final String PUBLISHED_DIR_NAME = "published";
//	private static final String TMP_DIR_NAME = "tmp";
//
//	private static final Map<String, SimpleDiskCache> instances = new HashMap<String, SimpleDiskCache>();
//
//	public static synchronized SimpleDiskCache open(File dir, int maxSize) throws IOException {
//		Preconditions.checkArgument(dir != null);
//		Preconditions.checkArgument(maxSize > 0);
//
//		String path = dir.getAbsolutePath();
//		if (!instances.containsKey(path)) {
//			FileUtils.forceMkdir(dir);
//
//			File publishedDir = new File(dir, PUBLISHED_DIR_NAME);
//			FileUtils.forceMkdir(publishedDir);
//
//			File tmpDir = new File(dir, TMP_DIR_NAME);
//			FileUtils.forceMkdir(tmpDir);
//
//			SimpleDiskCache cache = new SimpleDiskCache(dir, publishedDir, tmpDir, maxSize);
//			cache.initialize();
//
//			instances.put(path, cache);
//		}
//
//		return instances.get(path);
//	}
//
//	// ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
//
//	private final int maxSize;
//	private final File dir;
//	private final File publishedDir;
//	private final File tmpDir;
//	private final Random random = new Random();
//
//	private SimpleDiskCache(File dir, File publishedDir, File tmpDir, int maxSize) {
//		this.maxSize = maxSize;
//		this.dir = dir;
//		this.tmpDir = tmpDir;
//		this.publishedDir = publishedDir;
//	}
//
//	public InputStream getStream(String key) throws IOException {
//		Preconditions.checkArgument(key != null);
//
//		File file = fileFromKey(key);
//		synchronized (this) {
//			if (file.exists() && entries.get(hashedKey(key)) != null) {
//				return new FileInputStream(file);
//			} else {
//				file.delete();
//				return null;
//			}
//		}
//	}
//
//	public void putStream(String key, InputStream stream) throws IOException {
//		Preconditions.checkArgument(key != null);
//		Preconditions.checkArgument(stream != null);
//
//		// Save to tmpDir.
//		File tmpFile = newTmpFile();
//		long size = saveStreamToFile(stream, tmpFile);
//
//		publishTmpFile(key, tmpFile, size);
//	}
//
//	private synchronized void publishTmpFile(String key, File tmpFile, long size) throws IOException {
//		// Move possible existing from published/ to tmp/ and try to
//		// delete it. Deletion will silently fail if someone's reading it.
//		File publishedFile = fileFromKey(key);
//		if (publishedFile.exists()) {
//			File f = newTmpFile();
//			FileUtils.moveFile(publishedFile, f);
//			f.delete();
//		}
//
//		// Move from tmpDir to publishedDir.
//		FileUtils.moveFile(tmpFile, publishedFile);
//
//		// entries.put(hashedKey(key), size, System.currentTimeMillis());
//
//		trimIfNeeded();
//	}
//
//	private File fileFromKey(String key) {
//		return new File(publishedDir, String.valueOf(hashedKey(key)));
//	}
//
//	private long hashedKey(String key) {
//		int firstHalfLength = key.length() / 2;
//		int firstHalfHash = key.substring(0, firstHalfLength).hashCode();
//		int secondHalfHash = key.substring(firstHalfLength).hashCode();
//		return ((long) firstHalfHash << 32) | ((long) secondHalfHash & 0xFFFFFFFL);
//	}
//
//	private void initEntries() throws IOException {
//		Entries entries = new Entries(new File(dir, ENTRIES_FILE_NAME));
//		entries.initialize();
//	}
//
//	private void initialize() throws IOException {
//		FileUtils.cleanDirectory(tmpDir);
//		initEntries();
//		trimIfNeeded();
//	}
//
//	private File newTmpFile() {
//		String randomFileName = String.valueOf(random.nextLong());
//		return new File(tmpDir, randomFileName);
//	}
//
//	private int saveStreamToFile(InputStream stream, File file) throws IOException {
//		int bytes;
//
//		OutputStream outputStream = null;
//		try {
//			outputStream = new FileOutputStream(file);
//			bytes = IOUtils.copy(stream, outputStream);
//			if (bytes == -1) {
//				throw new IOException("The InputStream contained more than Integer.MAX_VALUE bytes.");
//			}
//		} finally {
//			IOUtils.closeQuietly(outputStream);
//		}
//
//		return bytes;
//	}
//
//	private synchronized void trim() {
//		// long totalSize = totalSize();
//		//
//		// Iterator<Map.Entry<String, Long>> iter = sizes.entrySet().iterator();
//		// while (iter.hasNext()) {
//		// if (totalSize < TRIM_RATIO * maxSize) break;
//		//
//		// Map.Entry<String, Long> entry = iter.next();
//		//
//		// if (fileFromKey(entry.getKey()).delete()) {
//		// totalSize -= entry.getValue();
//		// iter.remove();
//		// }
//		// }
//	}
//
//	private synchronized void trimIfNeeded() {
//		if (entries.totalSize() > maxSize) trim();
//	}

}

package cz.viic.android.merchant;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.TabbedPagerTool;
import cz.fhucho.android_utils.TabbedPagerTool.TabDescriptor;
import cz.viic.android.model.MerchantAppData;
import cz.viic.android.net.Api;
import cz.viic.android.net.NetworkListener;

public class MainActivity extends ActionBarActivity {

	private ScannerFragment scannerFragment = new ScannerFragment();
	private ProgramsFragment programsFragment = new ProgramsFragment();
	private StatsFragment statsFragment = new StatsFragment();

	public void loadData() {
		String merchantId = "d88ce7243b4a358ccf75881e19ffa84a";
		Api.getMerchantAppData(merchantId, new NetworkListener<MerchantAppData>() {
			@Override
			public void onSuccess(MerchantAppData data) {
				scannerFragment.onDataLoaded(data);
				statsFragment.onDataLoaded(data);
				if (!Settings.isLocked()) {
					programsFragment.onDataLoaded(data);
				}
			}

			@Override
			public void onError(VolleyError error) {
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		List<TabDescriptor> tabs = new ArrayList<TabDescriptor>();
		tabs.add(new TabDescriptor(scannerFragment, "Oskenovat"));
		tabs.add(new TabDescriptor(statsFragment, "Přehled"));
		if (!Settings.isLocked()) {
			tabs.add(new TabDescriptor(programsFragment, "Programy"));
		}
		TabbedPagerTool.init(this, tabs, R.id.pager);

		loadData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (Settings.isLocked()) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.main, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.unlock) {
			new UnlockDialog().show(this);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void restart() {
		scannerFragment.removeQrScannerView();

		Intent intent = getIntent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		finish();

		overridePendingTransition(0, 0);
		startActivity(intent);
	}

	public static class UnlockDialog extends android.support.v4.app.DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
			b.setTitle("Zadejte PIN");
			b.setView(getActivity().getLayoutInflater().inflate(R.layout.dialog_unlock, null));
			b.setPositiveButton("OK", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					ok();
				}
			});
			return b.create();
		}

		private void ok() {
			EditText pinView = (EditText) getDialog().findViewById(R.id.pin);
			String pin = pinView.getText().toString();
			if (pin.equals("1234")) {
				Settings.setLocked(false);
				((MainActivity) getActivity()).restart();
			} else {
				Toast.makeText(getActivity(), "Špatně", Toast.LENGTH_SHORT).show();
			}
		}

		public void show(FragmentActivity activity) {
			show(activity.getSupportFragmentManager(), null);
		}
	}
}
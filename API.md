### Viic API

[http://54.229.98.246](http://54.229.98.246)

**GET /customer_app_data?user_id=[user id]**

Example: [http://54.229.98.246/customer_app_data?user_id=7c6d1b2813f3b549742f95b60ba1ba53](http://54.229.98.246/customer_app_data?user_id=7c6d1b2813f3b549742f95b60ba1ba53)

**GET /merchant_app_data?merchant_id=[merchant id]**

Example: [http://54.229.98.246/merchant_app_data?merchant_id=d88ce7243b4a358ccf75881e19ffa84a](http://54.229.98.246/merchant_app_data?merchant_id=d88ce7243b4a358ccf75881e19ffa84a)

**PUT /programs/[id]**

Example PUT data:
```
{
	"unit":"koupenych zakusku",
	"title":"kava zdarma",
	"entry_bonus_title":"balonek",
	"merchant_id":"d88ce7243b4a358ccf75881e19ffa84a",
	"required_points":10,
	"entry_bonus_cost":7,
	"cost":15,
	"id":"ae8f1b967e149987ea461698b444bdfd"
}
```

**PUT /reg_ids/[id]**

Example PUT data:
```
{
	"reg_id": "APA91bFM2UUYmPk5Lot5HUIxoVFIQ-PckoUKoajZAVGKdKa31pXbxSuec1a2xcsXgKk6Iq0MTy3sJ9MSA8qacN3CYm5zz6QcWPBBpU-fcOfSCcvjVlFJYelIf0L_9-SE-7sL0DL5tgR60sucouwfaeAjfWh8khKeLFfdRL5R4vAifMqZK8PsujQ",
	"user_id":"e42c1703f1d670673ad5cde17547b4fc",
	"id":"f4d96a61c432d381e7280286234a00e0"
}
```

**PUT /user_merchant_assocs/[id]**

Example PUT data:
```
{
	"user_id":"3c1b7339d1eaec38eef820e8c0c4deb0",
	"merchant_id":"73d51e4292bf14965a822c6b8026be66",
	"last_paid_out":0,"entry_bonus_available":1,
	"points":0,"created":1396381794,
	"id":"5a20234eb71a97e16a17442969a12360"
}
```
package cz.viic.android.customer.app;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import cz.fhucho.android_utils.Random128BitId;
import cz.viic.android.app.App;

public class Settings {

	private static final SharedPreferences prefs;

	static {
		prefs = PreferenceManager.getDefaultSharedPreferences(App.get());
	}

	public static String getUserId() {
		String userId = prefs.getString("userId", null);
		if(userId == null) {
			userId = Random128BitId.generate();
			setString("userId", userId);
		}
		return userId;
	}

	public static String getGcmRegistrationId() {
		return prefs.getString("gcmRegistrationId", null);
	}

	public static void setGcmRegistrationId(String regId) {
		setString("gcmRegistrationId", regId);
	}
	
	private static void setString(String key, String value) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(key, value);
		editor.apply();
	}

}

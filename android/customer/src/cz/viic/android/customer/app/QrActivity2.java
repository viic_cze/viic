package cz.viic.android.customer.app;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import cz.fhucho.android_utils.TabbedPagerTool;
import cz.fhucho.android_utils.TabbedPagerTool.TabDescriptor;
import cz.viic.android.customer.R;

public class QrActivity2 extends ActionBarActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr);

		List<TabDescriptor> tabs = new ArrayList<TabDescriptor>();
		tabs.add(new TabDescriptor(new ShowQrFragment(), "Ukázat"));
		tabs.add(new TabDescriptor(new ScanQrFragment(), "Skenovat"));
		TabbedPagerTool.init(this, tabs, R.id.pager);
	}

	@Override
	protected void onResume() {
		super.onResume();

		IntentFilter filter = new IntentFilter("com.google.android.c2dm.intent.RECEIVE");
		filter.addCategory("com.example.gcm");

		registerReceiver(receiver, filter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}

	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();

			String message = intent.getStringExtra("message");
			Intent i = ConfirmationActivity.IntentTool.create(QrActivity2.this, message);
			startActivity(i);
		}
	};
	
}

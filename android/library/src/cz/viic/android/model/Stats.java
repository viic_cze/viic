package cz.viic.android.model;

public class Stats {
	private int paidOut;
	private int customersTotal;
	private int customersNew;
	private int customersServed;

	public int getPaidOut() {
		return paidOut;
	}

	public int getCustomersNew() {
		return customersNew;
	}

	public int getCustomersServed() {
		return customersServed;
	}

	public int getCustomersTotal() {
		return customersTotal;
	}
}

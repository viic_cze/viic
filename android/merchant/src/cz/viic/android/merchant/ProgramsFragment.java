package cz.viic.android.merchant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.BetterArrayAdapter;
import cz.fhucho.android_utils.Ui;
import cz.viic.android.app.ProgressDialogFragment;
import cz.viic.android.model.MerchantAppData;
import cz.viic.android.model.Program;
import cz.viic.android.net.ApiPutTask;

public class ProgramsFragment extends Fragment {
	private static final int REQUEST_ADD_PROGRAM = 0;

	private ProgramsAdapter adapter;

	private void addProgram(Program program) {
		ProgressDialogFragment.show("Odesílám", getActivity());
		new PutProgramTask(program).start(getActivity());
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				onActivityResultDelayed(requestCode, resultCode, data);
			}
		});
	}
	
	public void onActivityResultDelayed(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ADD_PROGRAM && resultCode == Activity.RESULT_OK) {
			addProgram(AddProgramActivity.ResultTool.getProgram(data));
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.programs, menu);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		adapter = new ProgramsAdapter();

		ListView listView = (ListView) inflater.inflate(R.layout.frag_programs, container,
				false);
		listView.addHeaderView(new View(getActivity()));
		listView.addFooterView(new View(getActivity()));
		listView.setAdapter(adapter);

		return listView;
	}

	public void onDataLoaded(MerchantAppData data) {
		adapter.setItems(data.getPrograms());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.add_program) {
			Intent intent = new Intent(getActivity(), AddProgramActivity.class);
			startActivityForResult(intent, REQUEST_ADD_PROGRAM);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private static class PutProgramTask extends ApiPutTask {
		public PutProgramTask(Program program) {
			super("programs", program);
		}

		@Override
		protected void onError(VolleyError error) {
			ProgressDialogFragment.dismiss(getActivity());
			Toast.makeText(getActivity(), "Chyba", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onSuccess() {
			ProgressDialogFragment.dismiss(getActivity());
			((MainActivity) getActivity()).loadData();
		}
	}

	private class ProgramsAdapter extends BetterArrayAdapter<Program> {
		public ProgramsAdapter() {
			super(getActivity(), R.layout.item_program);
		}

		@Override
		protected void setUpView(int position, Program program, View view) {
			Ui.textView(view, R.id.title).setText(program.getTitle());
		}
	}

}

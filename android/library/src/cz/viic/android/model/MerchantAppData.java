package cz.viic.android.model;

import java.util.List;

public class MerchantAppData {
	private List<Program> programs;
	private List<UserMerchantAssoc> userMerchantAssocs;
	private Stats stats;
	
	public Stats getStats() {
		return stats;
	}
	
	public List<Program> getPrograms() {
		return programs;
	}
	
	public List<UserMerchantAssoc> getUserMerchantAssocs() {
		return userMerchantAssocs;
	}
}

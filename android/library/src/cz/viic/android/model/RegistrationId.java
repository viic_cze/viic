package cz.viic.android.model;

import cz.fhucho.android_utils.Random128BitId;

@SuppressWarnings("serial")
public class RegistrationId extends Entity {
	private String userId;
	private String regId;

	public RegistrationId(String userId, String regId) {
		super(Random128BitId.generate());
		this.userId = userId;
		this.regId = regId;
	}
}

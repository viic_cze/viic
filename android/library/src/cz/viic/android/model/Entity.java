package cz.viic.android.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Entity implements Serializable {
	private String id;

	public Entity() {
	}

	public Entity(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}

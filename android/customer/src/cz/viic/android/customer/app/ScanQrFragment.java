package cz.viic.android.customer.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.Ui;
import cz.viic.android.app.ProgressDialogFragment;
import cz.viic.android.customer.R;
import cz.viic.android.model.UserMerchantAssoc;
import cz.viic.android.net.ApiPutTask;
import cz.viic.android.utils.QrScannerView;

public class ScanQrFragment extends Fragment implements QrScannerView.OnQrScannedListener {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_scan_qr, container, false);
		QrScannerView qrScanner = Ui.view(view, R.id.camera_preview);
		qrScanner.setOnQrScannedListener(this);
		return view;
	}

	@Override
	public void onQrScanned(String merchantId) {
		UserMerchantAssoc assoc = new UserMerchantAssoc(Settings.getUserId(), merchantId);
		new PutUserMerchantAssoc(assoc).start(getActivity());
	}
	
	private static class PutUserMerchantAssoc extends ApiPutTask {
		public PutUserMerchantAssoc(UserMerchantAssoc assoc) {
			super("user_merchant_assocs", assoc);
		}

		@Override
		protected void onError(VolleyError error) {
			ProgressDialogFragment.dismiss(getActivity());
			Toast.makeText(getActivity(), "Chyba", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onSuccess() {
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}
}

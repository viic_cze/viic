package cz.viic.android.customer.app;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.TabbedPagerTool;
import cz.fhucho.android_utils.TabbedPagerTool.TabDescriptor;
import cz.viic.android.customer.R;
import cz.viic.android.model.CustomerAppData;
import cz.viic.android.model.Merchant;
import cz.viic.android.net.Api;
import cz.viic.android.net.NetworkListener;

public class MerchantActivity extends ActionBarActivity {
	
	private Merchant merchant;
	private MerchantFragment merchantFragment;

	public void loadData() {
		String userId = Settings.getUserId();
		Api.getCustomerAppData(userId, new NetworkListener<CustomerAppData>() {
			@Override
			public void onSuccess(CustomerAppData data) {
				for (Merchant m : data.getMerchants()) {
					if (m.getId().equals(merchant.getId())) {
						merchantFragment.setMerchant(m);
					}
				}
			}

			@Override
			public void onError(VolleyError error) {
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant);

		merchant = IntentTool.getMerchant(getIntent());
		setUpActionBar(merchant);
		
		merchantFragment = new MerchantFragment();
		merchantFragment.setArguments(getIntent().getExtras());
		
		MapFragment mapFragment = new MapFragment();
		Bundle b = new Bundle();
		b.putString("merchantName", merchant.getName());
		mapFragment.setArguments(b);
		
		List<TabDescriptor> tabs = new ArrayList<TabDescriptor>();
		tabs.add(new TabDescriptor(merchantFragment, "Info"));
		tabs.add(new TabDescriptor(mapFragment, "Mapa"));
		TabbedPagerTool.init(this, tabs, R.id.pager);
	}

	private void setUpActionBar(Merchant merchant) {
		ActionBar ab = getSupportActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
		ab.setTitle(merchant.getName());
	}

	public static class IntentTool {
		public static Intent create(Context c, Merchant merchant) {
			Intent i = new Intent(c, MerchantActivity.class);
			i.putExtra("merchant", merchant);
			return i;
		}

		private static Merchant getMerchant(Intent intent) {
			return (Merchant) intent.getSerializableExtra("merchant");
		}
	}
}

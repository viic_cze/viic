package cz.viic.android.net;

import java.lang.reflect.Type;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class ApiGetRequest<T> extends ApiRequest<T> {
	private static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(
			FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

	private final Type typeOfT;

	public ApiGetRequest(String urlSuffix, NetworkListener<T> listener, Type typeOfT) {
		super(Method.GET, URL_PREFIX + urlSuffix, listener);
		this.typeOfT = typeOfT;
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data);
			T result = GSON.fromJson(json, typeOfT);

			Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
			cacheEntry.ttl = Long.MAX_VALUE;

			return Response.success(result, cacheEntry);
		} catch (JsonSyntaxException e) {
			return Response.error(new ParseError(e));
		}
	}
}
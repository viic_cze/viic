package cz.fhucho.workers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

import android.os.Handler;

public class HandlerProxy {
	public static <T> T createProxy(List<T> listeners, Class<T> type, Handler handler) {
		InvocationHandler invocationHandler = new MyInvocationHandler<T>(listeners, handler);
		Class<?>[] interfaces = new Class<?>[] { type };
		Object proxy = Proxy.newProxyInstance(type.getClassLoader(), interfaces, invocationHandler);
		
		return type.cast(proxy);
	}

	private static class MyInvocationHandler<T> implements InvocationHandler {
		private final Handler handler;
		private final List<T> listeners;

		public MyInvocationHandler(List<T> listeners, Handler handler) {
			this.listeners = listeners;
			this.handler = handler;
		}

		@Override
		public Object invoke(Object proxy, final Method method, final Object[] args)
				throws Throwable {
			handler.post(new Runnable() {
				@Override
				public void run() {
					for (T listener : listeners) {
						invokeListener(listener, method, args);
					}
				}
			});
			return null;
		}

		private void invokeListener(T listener, Method method, Object[] args) {
			try {
				method.invoke(listener, args);
			} catch (IllegalArgumentException e) {
				throw new AssertionError();
			} catch (IllegalAccessException e) {
				throw new AssertionError();
			} catch (InvocationTargetException e) {
				throw new RuntimeException("Listener threw an exception", e);
			}
		}
	}
}

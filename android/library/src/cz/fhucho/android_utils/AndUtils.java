package cz.fhucho.android_utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class AndUtils {
	public static boolean apiGreaterOrEqual(int apiLevel) {
		return Build.VERSION.SDK_INT >= apiLevel;
	}

	public static String getDeviceId(Context c) {
		TelephonyManager m = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
		return m.getDeviceId();
	}

	public static int getVersionCode(Context c) {
		try {
			return c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			throw new AssertionError();
		}
	}

	public static String getVersionName(Context c) {
		try {
			String pkg = c.getPackageName();
			return c.getPackageManager().getPackageInfo(pkg, 0).versionName;
		} catch (NameNotFoundException e) {
			throw new AssertionError();
		}
	}

	public static void openUrlInBrowser(String url, Context c) {
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		c.startActivity(intent);
	}

	public static void openVideoUrl(String url, Context c) {
		Uri uri = Uri.parse(url);
		Intent i = new Intent("android.intent.action.VIEW");
		i.setDataAndType(uri, "video/*");
		c.startActivity(i);
	}

	public static void setCameraDisplayOrientation(Activity activity, int cameraId,
			Camera camera) {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // Compensate the mirror
		} else { // Back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}

	public static void shortToast(String message, Context context) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public static View verticalSpace(Context context, int dimensionResId) {
		int height = context.getResources().getDimensionPixelSize(dimensionResId);

		View space = new View(context);
		space.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				height));

		return space;
	}
}

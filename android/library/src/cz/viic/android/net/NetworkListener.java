package cz.viic.android.net;

import com.android.volley.VolleyError;

public abstract class NetworkListener<T> {

	public abstract void onSuccess(T result);

	public abstract void onError(VolleyError arg0);

}

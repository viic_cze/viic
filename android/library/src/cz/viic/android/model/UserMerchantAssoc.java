package cz.viic.android.model;

import java.io.Serializable;

import cz.fhucho.android_utils.Random128BitId;

@SuppressWarnings("serial")
public class UserMerchantAssoc extends Entity implements Serializable {
	private int created;
	private int entryBonusAvailable;
	private int lastPaidOut;
	private int points;
	private String userId;
	private String merchantId;

	public UserMerchantAssoc(String userId, String merchantId) {
		super(Random128BitId.generate());
		this.userId = userId;
		this.merchantId = merchantId;
		this.created = (int) (System.currentTimeMillis() / 1000);
		this.entryBonusAvailable = 1;
		this.points = 0;
		this.lastPaidOut = 0;
	}

	public UserMerchantAssoc(UserMerchantAssoc assoc) {
		super(assoc.getId());
		this.userId = assoc.userId;
		this.merchantId = assoc.merchantId;
		this.points = assoc.points;
		this.created = assoc.created;
		this.lastPaidOut = assoc.lastPaidOut;
	}

	public int getPoints() {
		return points;
	}

	public String getUserId() {
		return userId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public boolean isEntryBonusAvailable() {
		return entryBonusAvailable == 1;
	}

	public void setLastPaidOut(int lastPaidOut) {
		this.lastPaidOut = lastPaidOut;
	}

	public void setPoints(int points) {
		this.points = points;
	}
}

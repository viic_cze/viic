package cz.viic.android.merchant;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import cz.viic.android.app.App;

public class Settings {
	
	private static final SharedPreferences prefs;
	
	static {
		prefs = PreferenceManager.getDefaultSharedPreferences(App.get());
	}
	
	public static boolean isLocked() {
		return prefs.getBoolean("locked", true);
	}

	public static void setLocked(boolean locked) {
		Editor editor = prefs.edit();
		editor.putBoolean("locked", locked);
		editor.apply();
	}
}

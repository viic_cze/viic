package cz.viic.android.model;

import java.io.Serializable;

import cz.fhucho.android_utils.Random128BitId;

@SuppressWarnings("serial")
public class Program extends Entity implements Serializable {
	private int cost;
	private int entryBonusCost;
	private int requiredPoints;
	private String entryBonusTitle;
	private String merchantId;
	private String title;
	private String unit;

	public Program() {
	}

	public Program(String merchantId, String title, int requiredPoints, String unit, int cost,
			String entryBonusTitle, int entryBonusCost) {
		super(Random128BitId.generate());
		this.requiredPoints = requiredPoints;
		this.merchantId = merchantId;
		this.title = title;
		this.unit = unit;
		this.cost = cost;
		this.entryBonusTitle = entryBonusTitle;
		this.entryBonusCost = entryBonusCost;
	}
	
	public int getCost() {
		return cost;
	}
	
	public int getEntryBonusCost() {
		return entryBonusCost;
	}
	
	public String getEntryBonusTitle() {
		return entryBonusTitle;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public int getRequiredPoints() {
		return requiredPoints;
	}

	public String getTitle() {
		return title;
	}
	
	public String getUnit() {
		return unit;
	}

}

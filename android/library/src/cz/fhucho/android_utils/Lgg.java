package cz.fhucho.android_utils;

import android.util.Log;

public class Lgg {
	private final String tag;

	public Lgg(String tag) {
		this.tag = tag;
	}

	public Lgg(Lgg parentLg, String tag) {
		this.tag = parentLg.tag + "." + tag;
	}

	public void v(String msg) {
		Log.v(tag, msg);
	}

	public void v(String msg, Exception e) {
		Log.v(tag, msg, e);
	}

	public void d(String msg) {
		Log.d(tag, msg);
	}

	public void d(String msg, Exception e) {
		Log.d(tag, msg, e);
	}

	public void i(String msg) {
		Log.i(tag, msg);
	}

	public void i(String msg, Exception e) {
		Log.i(tag, msg, e);
	}

	public void w(String msg) {
		Log.w(tag, msg);
	}

	public void w(String msg, Exception e) {
		Log.w(tag, msg, e);
	}

	public void e(String msg) {
		Log.e(tag, msg);
	}

	public void e(String msg, Exception e) {
		Log.e(tag, msg, e);
	}

	public void wtf(String msg) {
		Log.wtf(tag, msg);
	}

	public void wtf(String msg, Exception e) {
		Log.wtf(tag, msg, e);
	}

}

package cz.viic.android.customer.app;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import cz.fhucho.android_utils.Ui;
import cz.viic.android.customer.R;
import cz.viic.android.customer.app.MyImageView.OnSizeChangedListener;

public class QrActivity extends ActionBarActivity {
	private MyImageView qrView;

	private void loadQrCode(int size) {
		String merchantId = IntentTool.getMerchantId(getIntent());
		byte[][] bits = textToQrBits(merchantId);
		qrView.setImageBitmap(bitmapFromQrBits(bits, size));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_program);

		qrView = Ui.view(this, R.id.qr_code);
		qrView.setOnSizeChangedListener(new OnSizeChangedListener() {
			@Override
			public void onSizeChanged(int w, int h, int oldw, int oldh) {
				if (w != oldw) {
					loadQrCode(w);
				}
			}
		});
	}

	private static Bitmap bitmapFromQrBits(byte[][] bits, int size) {
		Paint white = new Paint();
		white.setColor(0x00000000);

		Paint black = new Paint();
		black.setColor(0xFF000000);

		Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		Rect rect = new Rect();
		int bitsSize = bits.length;

		for (int x = 0; x < bitsSize; x++) {
			for (int y = 0; y < bitsSize; y++) {
				rect.left = (int) (((float) x / bitsSize) * size);
				rect.right = (int) (((float) (x + 1) / bitsSize) * size);
				rect.top = (int) (((float) y / bitsSize) * size);
				rect.bottom = (int) (((float) (y + 1) / bitsSize) * size);
				canvas.drawRect(rect, bits[x][y] == 0 ? white : black);
			}
		}

		return bitmap;
	}

	private static byte[][] textToQrBits(String text) {
		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

		try {
			QRCode code = Encoder.encode(text, ErrorCorrectionLevel.M, hints);
			return code.getMatrix().getArray();
		} catch (WriterException e) {
			throw new AssertionError();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		IntentFilter filter = new IntentFilter("com.google.android.c2dm.intent.RECEIVE");
		filter.addCategory("com.example.gcm");

		registerReceiver(receiver, filter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}

	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();

			String message = intent.getStringExtra("message");
			Intent i = ConfirmationActivity.IntentTool.create(QrActivity.this, message);
			startActivity(i);
		}
	};
	
	public static class IntentTool {
		public static Intent create(Context context, String merchantId) {
			Intent intent = new Intent(context, QrActivity.class);
			intent.putExtra("merchantId", merchantId);
			return intent;
		}

		public static String getMerchantId(Intent intent) {
			return intent.getStringExtra("merchantId");
		}
	}
}

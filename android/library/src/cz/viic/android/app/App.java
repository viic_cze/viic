package cz.viic.android.app;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class App extends Application {
	private static App app;
	private static RequestQueue volleyQueue;

	@Override
	public void onCreate() {
		super.onCreate();
		app = this;
		volleyQueue = Volley.newRequestQueue(this);
	}

	public static App get() {
		return app;
	}

	public static RequestQueue getVolleyQueue() {
		return volleyQueue;
	}
}

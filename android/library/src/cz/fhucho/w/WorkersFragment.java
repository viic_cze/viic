package cz.fhucho.w;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class WorkersFragment extends Fragment {
	private static final String TAG = "workers-fragment";
	private final Set<Worker> workers = new HashSet<Worker>();
	private boolean destroyed = false;

	public WorkersFragment() {
		setRetainInstance(true);
	}

	public void addWorker(Worker worker) {
		if (!isAdded()) {
			throw new IllegalStateException("The fragment is not added to the activity");
		}

		if (workers.contains(worker)) {
			throw new RuntimeException("This worker is already here");
		}

		workers.add(worker);
	}

	public void removeWorker(Worker worker) {
		if (!workers.contains(worker)) {
			throw new RuntimeException("This worker is not here");
		}

		workers.remove(worker);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		for (Worker w : workers) {
			w.attachActivity((FragmentActivity) activity);
			w.onAttached();
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();

		for (Worker w : workers) {
			w.detachActivity();
			w.onDetached();
			if (destroyed) w.onDestroy();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		destroyed = true;
	}

	public static WorkersFragment getOrCreate(FragmentActivity activity) {
		FragmentManager fm = activity.getSupportFragmentManager();
		WorkersFragment frag = (WorkersFragment) fm.findFragmentByTag(WorkersFragment.TAG);
		if (frag == null) {
			frag = new WorkersFragment();
			fm.beginTransaction().add(frag, WorkersFragment.TAG).commitAllowingStateLoss();
			fm.executePendingTransactions();
		}

		return frag;
	}
}

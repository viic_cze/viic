package cz.viic.android.utils;

import java.io.IOException;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class QrScannerView extends SurfaceView implements SurfaceHolder.Callback,
		Camera.PreviewCallback {
	static {
		System.loadLibrary("iconv");
	}

	private Camera camera;
	private ImageScanner scanner;
	private SurfaceHolder holder;

	private OnQrScannedListener listener;

	public QrScannerView(Context context, AttributeSet attrs) {
		super(context, attrs);

		holder = getHolder();
		holder.addCallback(this);
		// Deprecated but required on Android versions prior to 3.0
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		Camera.Size size = camera.getParameters().getPreviewSize();

		Image barcode = new Image(size.width, size.height, "Y800");
		barcode.setData(data);

		if (scanner.scanImage(barcode) != 0) {
			if (listener != null) {
				camera.setPreviewCallback(null);
				camera.stopPreview();
				listener.onQrScanned(scanner.getResults().iterator().next().getData());
			}
		}
	}

	public void setOnQrScannedListener(OnQrScannedListener listener) {
		this.listener = listener;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO 1) background 2) null 3) RntmExcptn
		camera = Camera.open(CameraInfo.CAMERA_FACING_BACK);
		camera.setDisplayOrientation(90); // TODO
		camera.startPreview();
		camera.setPreviewCallback(this);

		Camera.Parameters params = camera.getParameters();
		params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
		camera.setParameters(params);

		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException e) {
			// According to doc, IOException is thrown if the surface is not
			// available, which should never be the case.
			throw new AssertionError();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		camera.setPreviewCallback(null);
		camera.stopPreview();
		camera.release();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
	}

	public interface OnQrScannedListener {
		public void onQrScanned(String data);
	}

}
package cz.viic.android.merchant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.Ui;
import cz.viic.android.app.ProgressDialogFragment;
import cz.viic.android.model.Program;
import cz.viic.android.model.UserMerchantAssoc;
import cz.viic.android.net.ApiPutTask;

public class UserActivity extends ActionBarActivity {
	private int addedPoints = 0;
	private Program program;
	private UserMerchantAssoc assoc;

	public void addPoint(View view) {
		setAddedPoints(addedPoints + 1);
	}

	private int countSelectedBenefits() {
		int count = 0;
		ViewGroup container = Ui.view(this, R.id.benefits);
		for (int i = 0; i < container.getChildCount(); i++) {
			if (container.getChildAt(i).isSelected()) {
				count++;
			}
		}
		return count;
	}

	public void onBenefitClicked(View view) {
		view.setSelected(!view.isSelected());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user);

		assoc = IntentTool.getUserMerchantAssoc(getIntent());
		program = IntentTool.getProgram(getIntent());

		setAddedPoints(0);

		Ui.bindClick(this, R.id.add_point, this, "addPoint");
		Ui.bindClick(this, R.id.remove_point, this, "removePoint");
		Ui.textView(this, R.id.next_benefit_title).setText(program.getTitle());
	}

	public void removePoint(View view) {
		if (addedPoints == 0) return;
		setAddedPoints(addedPoints - 1);
	}

	private void setBenefitsBottomSpaceVisibility(int visibility) {
		ViewGroup benefitsContainer = Ui.view(this, R.id.benefits);
		View last = benefitsContainer.getChildAt(benefitsContainer.getChildCount() - 1);
		if (last != null) {
			Ui.view(last, R.id.bottom_space).setVisibility(visibility);
		}
	}

	private void setAddedPoints(int addedPoints) {
		this.addedPoints = addedPoints;
		Ui.textView(this, R.id.points).setText(String.valueOf(addedPoints));
		updateAvailableBenefits();
		updateNextBenefit();
	}

	public void submit(View view) {
		UserMerchantAssoc updatedAssoc = new UserMerchantAssoc(assoc);
		updatedAssoc.setPoints(assoc.getPoints() + addedPoints - countSelectedBenefits()
				* program.getRequiredPoints());
		updatedAssoc.setLastPaidOut(countSelectedBenefits() * program.getCost());
		new PutUserMerchantAssoc(updatedAssoc).start(this);

		ProgressDialogFragment.show("Odesílám", this);
	}

	private void updateAvailableBenefits() {
		int availableBenefits = (assoc.getPoints() + addedPoints)
				/ program.getRequiredPoints();

		ViewGroup container = Ui.view(this, R.id.benefits);
		while (container.getChildCount() > availableBenefits) {
			container.removeViewAt(container.getChildCount() - 1);
		}

		setBenefitsBottomSpaceVisibility(View.VISIBLE);

		LayoutInflater inflater = getLayoutInflater();
		while (container.getChildCount() < availableBenefits) {
			View benefit = inflater.inflate(R.layout.item_benefit, container, false);
			Ui.textView(benefit, R.id.title).setText(program.getTitle());
			Ui.bindClick(benefit, this, "onBenefitClicked");
			container.addView(benefit);
		}

		setBenefitsBottomSpaceVisibility(View.GONE);

		int titleVisibility = container.getChildCount() == 0 ? View.GONE : View.VISIBLE;
		Ui.view(this, R.id.benefits_title).setVisibility(titleVisibility);
	}

	private void updateNextBenefit() {
		int requiredPoints = program.getRequiredPoints();
		int progress = (assoc.getPoints() + addedPoints) % requiredPoints;
		Ui.textView(this, R.id.next_benefit_progress).setText(
				progress + " / " + requiredPoints);
	}

	private static class PutUserMerchantAssoc extends ApiPutTask {
		public PutUserMerchantAssoc(UserMerchantAssoc assoc) {
			super("user_merchant_assocs", assoc);
		}

		@Override
		protected void onError(VolleyError error) {
			ProgressDialogFragment.dismiss(getActivity());
			Toast.makeText(getActivity(), "Chyba", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onSuccess() {
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}

	public static class IntentTool {
		public static Intent create(Context context, UserMerchantAssoc userMerchantAssoc,
				Program program) {
			Intent intent = new Intent(context, UserActivity.class);
			intent.putExtra("userMerchantAssoc", userMerchantAssoc);
			intent.putExtra("program", program);
			return intent;
		}

		public static Program getProgram(Intent intent) {
			return (Program) intent.getSerializableExtra("program");
		}

		public static UserMerchantAssoc getUserMerchantAssoc(Intent intent) {
			return (UserMerchantAssoc) intent.getSerializableExtra("userMerchantAssoc");
		}
	}
}

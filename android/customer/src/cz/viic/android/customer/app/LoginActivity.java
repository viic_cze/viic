package cz.viic.android.customer.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import cz.fhucho.android_utils.Ui;
import cz.fhucho.w.Worker;
import cz.viic.android.app.ProgressDialogFragment;
import cz.viic.android.customer.R;
import cz.viic.android.model.User;
import cz.viic.android.net.Api;
import cz.viic.android.net.NetworkListener;

public class LoginActivity extends ActionBarActivity {
//	private static final String TAG_LOGGING_IN_DIALOG = "logging-in";
//	private Ui ui;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_login);
//		ui = new Ui(this);
//	}
//
//	public void login(View view) {
//		String email = ui.textView(R.id.email).getText().toString();
//		String password = ui.textView(R.id.password).getText().toString();
//
//		Settings settings = Settings.get();
//		settings.setUsernameAndPassword(email, password);
//		settings.save();
//
//		ProgressDialogFragment.show("Přihlašování", this);
//
//		new LoginTask(email).start(this);
//	}
//
//	private void onLogInSuccess() {
//		ProgressDialogFragment.dismiss(this);
//
//		Settings settings = Settings.get();
//		settings.setLoggedIn(true);
//		settings.save();
//
//		setResult(RESULT_OK);
//		finish();
//	}
//
//	private void onLogInError() {
//		ProgressDialogFragment.dismiss(this);
//		Toast.makeText(LoginActivity.this, "Chyba", Toast.LENGTH_SHORT).show();
//	}
//
//	public static class LoginTask extends Worker {
//		private final String username;
//
//		private Request<?> request;
//
//		private User resultSuccess;
//		private VolleyError resultError;
//
//		public LoginTask(String username) {
//			this.username = username;
//		}
//
//		@Override
//		public void onCreate() {
//			request = Api.getUser(username, new NetworkListener<User>() {
//				@Override
//				public void onSuccess(User user) {
//					resultSuccess = user;
//					if (getActivity() != null) deliverResult();
//				}
//
//				@Override
//				public void onError(VolleyError error) {
//					resultError = error;
//					if (getActivity() != null) deliverResult();
//				}
//			});
//		}
//
//		@Override
//		public void onAttached() {
//			if (resultSuccess != null || resultError != null) deliverResult();
//		}
//
//		@Override
//		public void onDestroy() {
//			request.cancel();
//		}
//
//		private void deliverResult() {
//			if (resultSuccess != null) {
//				((LoginActivity) getActivity()).onLogInSuccess();
//			} else if (resultError != null) {
//				((LoginActivity) getActivity()).onLogInError();
//			} else {
//				throw new AssertionError();
//			}
//
//			destroy();
//		}
//
//	}
}

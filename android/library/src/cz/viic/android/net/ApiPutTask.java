package cz.viic.android.net;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import cz.fhucho.w.Worker;
import cz.viic.android.app.App;
import cz.viic.android.model.Entity;

public abstract class ApiPutTask extends Worker {
	private Request<?> request;

	private Object resultSuccess;
	private VolleyError resultError;

	public ApiPutTask(String collection, Entity entity) {
		NetworkListener<Object> listener = new NetworkListener<Object>() {
			@Override
			public void onSuccess(Object result) {
				resultSuccess = result;
				if (getActivity() != null) deliverResult();
			}

			@Override
			public void onError(VolleyError error) {
				resultError = error;
				if (getActivity() != null) deliverResult();
			}
		};
		request = new ApiPutRequest(collection, entity, listener);
	}

	@Override
	public void onCreate() {
		App.getVolleyQueue().add(request);
	}

	@Override
	public void onAttached() {
		if (resultSuccess != null || resultError != null) deliverResult();
	}

	@Override
	public void onDestroy() {
		request.cancel();
	}

	private void deliverResult() {
		if (resultSuccess != null) {
			onSuccess();
		} else if (resultError != null) {
			onError(resultError);
		} else {
			throw new AssertionError();
		}

		destroy();
	}

	protected abstract void onError(VolleyError error);

	protected abstract void onSuccess();
}
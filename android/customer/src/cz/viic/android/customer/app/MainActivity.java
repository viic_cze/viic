package cz.viic.android.customer.app;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.TabbedPagerTool;
import cz.fhucho.android_utils.TabbedPagerTool.TabDescriptor;
import cz.viic.android.customer.R;
import cz.viic.android.model.CustomerAppData;
import cz.viic.android.net.Api;
import cz.viic.android.net.NetworkListener;

public class MainActivity extends ActionBarActivity {
	private static final int REQUEST_QR = 0;

	private NearbyFragment nearbyFragment = new NearbyFragment();
	private FavouritesFragment favouritesFragment = new FavouritesFragment();

	public void loadData() {
		String userId = Settings.getUserId();
		System.out.println(userId);
		Api.getCustomerAppData(userId, new NetworkListener<CustomerAppData>() {
			@Override
			public void onSuccess(CustomerAppData data) {
				nearbyFragment.onDataLoaded(data);
				favouritesFragment.onDataLoaded(data);
			}

			@Override
			public void onError(VolleyError error) {
				error.getCause().printStackTrace();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_QR) {
			loadData();
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		List<TabDescriptor> tabs = new ArrayList<TabDescriptor>();
		tabs.add(new TabDescriptor(nearbyFragment, "V okolí"));
		tabs.add(new TabDescriptor(favouritesFragment, "Moje"));
		TabbedPagerTool.init(this, tabs, R.id.pager);

		loadData();

		GcmTool.registerIfNeeded();
	}

	public void onQrClicked(View view) {
		startActivityForResult(new Intent(this, QrActivity2.class), REQUEST_QR);
	}

}

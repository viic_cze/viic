package cz.fhucho.simple_disk_cache;

public class Preconditions {
	private Preconditions() {
	}

	public static void checkArgument(boolean expression) {
		if (!expression) {
			throw new IllegalArgumentException();
		}
	}
}

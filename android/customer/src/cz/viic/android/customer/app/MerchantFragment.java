package cz.viic.android.customer.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import cz.fhucho.android_utils.Ui;
import cz.viic.android.app.ProgressDialogFragment;
import cz.viic.android.customer.R;
import cz.viic.android.model.Merchant;
import cz.viic.android.model.UserMerchantAssoc;
import cz.viic.android.net.ApiPutTask;

public class MerchantFragment extends Fragment {
	private Merchant merchant;

	public void enter() {
		String userId = Settings.getUserId();
		UserMerchantAssoc assoc = new UserMerchantAssoc(userId, merchant.getId());
		new PutUserMerchantAssoc(assoc).start(getActivity());

		ProgressDialogFragment.show("Odesílám", getActivity());
	}

	public void recommend() {
		startActivity(QrActivity.IntentTool.create(getActivity(), merchant.getId()));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_merchant, container, false);
		
		Ui.view(view, R.id.enter).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				enter();
			}
		});
		
		Ui.view(view, R.id.recommend).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				recommend();
			}
		});

		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Merchant merchant = (Merchant) getArguments().getSerializable("merchant");
		setMerchant(merchant);
	}

	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;

		ViewGroup container = Ui.view(getView(), R.id.benefits);
		BenefitsTool.setUpBenefits(container, merchant, true);

		boolean isMember = merchant.getUserMerchantAssoc() != null;
		Ui.view(getView(), R.id.enter).setVisibility(isMember ? View.GONE : View.VISIBLE);
		Ui.view(getView(), R.id.recommend).setVisibility(isMember ? View.VISIBLE : View.GONE);
	}

	private static class PutUserMerchantAssoc extends ApiPutTask {
		public PutUserMerchantAssoc(UserMerchantAssoc assoc) {
			super("user_merchant_assocs", assoc);
		}

		@Override
		protected void onError(VolleyError error) {
			ProgressDialogFragment.dismiss(getActivity());
			Toast.makeText(getActivity(), "Chyba", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onSuccess() {
			ProgressDialogFragment.dismiss(getActivity());
			((MerchantActivity) getActivity()).loadData();
		}
	}
}

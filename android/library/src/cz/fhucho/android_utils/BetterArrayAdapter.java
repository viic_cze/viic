package cz.fhucho.android_utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public abstract class BetterArrayAdapter<T> extends ArrayAdapter<T> {
	private final int itemLayout;

	public BetterArrayAdapter(Context context, int itemLayout) {
		this(context, itemLayout, new ArrayList<T>());
	}
	
	public BetterArrayAdapter(Context context, int itemLayout, List<T> objects) {
		super(context, itemLayout, objects);
		this.itemLayout = itemLayout;
	}
	
	public void setItems(Collection<? extends T> items) {
		clear();
		for (T item : items) {
			add(item);
		}
	}

	protected abstract void setUpView(int position, T item, View view);

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(itemLayout, parent, false);
		}
		setUpView(position, getItem(position), convertView);
		return convertView;
	}
}

package cz.viic.android.customer.app;

import java.io.IOException;

import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import cz.viic.android.app.App;
import cz.viic.android.model.Entity;
import cz.viic.android.model.RegistrationId;
import cz.viic.android.net.Api;
import cz.viic.android.net.NetworkListener;

public class GcmTool {
	private static final String SENDER_ID = "886599538346";

	public static void registerIfNeeded() {
		if (Settings.getGcmRegistrationId() == null) {
			new Thread(registerRunnable).start();
		}
	}
	
	private static void sendRegistrationIdToBackend(String regId) {
		Entity entity = new RegistrationId(Settings.getUserId(), regId);
		Api.put("reg_ids", entity, new NetworkListener<Object>() {
			@Override
			public void onSuccess(Object result) {
			}
			
			@Override
			public void onError(VolleyError arg0) {
			}
		});
	}

	private static final Runnable registerRunnable = new Runnable() {
		@Override
		public void run() {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(App.get());
			try {
				String regId = gcm.register(SENDER_ID);
				Settings.setGcmRegistrationId(regId);
				sendRegistrationIdToBackend(regId);
			} catch (IOException e) {
				// Ignore.
			}
		}
	};
}

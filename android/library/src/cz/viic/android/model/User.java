package cz.viic.android.model;

import cz.fhucho.android_utils.Random128BitId;

@SuppressWarnings("serial")
public class User extends Entity {
	private String username;
	private String password;

	public User() {
	}

	public User(String username, String password) {
		super(Random128BitId.generate());
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}

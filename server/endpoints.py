import json
import requests
import time

from db import *
from flask import *

def send_to_device(reg_id, message):
	url = 'https://android.googleapis.com/gcm/send'
	data = {'registration_ids': [reg_id], 'data': {'message': message}}
	headers = {'Authorization': 'key=AIzaSyD0bJcj76amW3rPU2CwyJ6NR23e8tBmAfQ', 'Content-Type': 'application/json'}
	requests.post(url, data=json.dumps(data), headers=headers)


def groupby(items, key):
	groups = {}
	for item in items:
		k = key(item)
		if not k in groups:
			groups[k] = []
		groups[k].append(item)
	return groups


def json_response(data):
	return Response(
		response=json.dumps(data, indent=2, sort_keys=True, ensure_ascii=False),
		mimetype='application/json')


def get_authenticated_user():
	auth = request.authorization
	print(auth)
	if auth != None:
		users = query("select * from users where username = ? and password = ?", auth.username, auth.password)
		if len(users) == 1:
			return users[0]
	return None


def get_fields():
	fields = request.args.get('fields')
	if fields == None:
		return []
	else:
		return fields.split(',')


def groupby(items, key):
	groups = {}
	for item in items:
		k = key(item)
		if not k in groups:
			groups[k] = []
		groups[k].append(item)
	return groups


def include_children(parents, children, parent_key, child_key, field):
	grouped_children = groupby(children, lambda c: c[child_key])
	for p in parents:
		p[field] = grouped_children.get(p[parent_key], [])


def get_stats():
	customers_total = len(query("select * from user_merchant_assocs"))
	customers_new = len(query("select * from user_merchant_assocs"))
	customers_served = len(query("select * from transactions"))
	paid_out = query("select sum(paid_out) from transactions")[0]['sum(paid_out)']
	return {'paid_out': paid_out, 'customers_total': customers_total, 'customers_new': customers_new, 'customers_served': customers_served}


def get_customer_app_data():
	user_id = request.args.get('user_id')
	merchants = query("select * from merchants")
	programs = query("select * from programs")
	user_merchant_assocs = query("select * from user_merchant_assocs where user_id = ?", user_id)
	include_children(merchants, programs, 'id', 'merchant_id', 'programs')
	include_children(merchants, user_merchant_assocs, 'id', 'merchant_id', 'user_merchant_assocs')
	return json_response({'merchants': merchants})


def get_merchant_app_data():
	merchant_id = request.args.get('merchant_id')
	user_merchant_assocs = query("select * from user_merchant_assocs where merchant_id = ?", merchant_id)
	programs = query("select * from programs where merchant_id = ?", merchant_id)
	stats = get_stats()
	return json_response({'user_merchant_assocs': user_merchant_assocs, 'programs': programs, 'stats': stats})


def get_merchants():
	include_programs = 'programs' in get_fields() or 'programs(subscriptions)' in get_fields()
	include_subscriptions = 'programs(subscriptions)' in get_fields()
	username = request.args.get('programs.subscriptions.username')
	
	merchants = query("select * from merchants")
	
	if include_programs:
		programs = query("select * from programs")
		include_children(merchants, programs, 'id', 'merchant_id', 'programs')
		
		if include_subscriptions:
			subscriptions = None
			if username == None:
				subscriptions = query("select * from subscriptions")
			else:
				subscriptions = query("select * from subscriptions where username = ?", username)
		
			include_children(programs, subscriptions, 'id', 'program_id', 'subscriptions')
	
	return json_response(merchants)


def put_user_merchant_assoc(id):
	data = request.get_json(force=True)
	
	old_points = 0
	assocs = query("select * from user_merchant_assocs where id = ?", id)
	if len(assocs) == 1:
		old_points = assocs[0]['points']
	points_diff = data['points'] - old_points

	insert_or_replace('user_merchant_assocs', data)

	if data['points'] > 0 or data['last_paid_out'] > 0:
		insert_or_replace('transactions', {'merchant_id': data['merchant_id'], 'paid_out': data['last_paid_out'], 'time': int(time.time()), 'user_id': data['user_id']})
	
	reg_id = query("select * from reg_ids where user_id = ?", data['user_id'])[0]['reg_id']
	text = 'Bylo vám připsáno ' + str(points_diff) + ' bodů.'
	send_to_device(reg_id, text)

	return json_response(data)


def put(collection, id):
	data = request.get_json(force=True)
	insert_or_replace(collection, data)
	return json_response(data)

package cz.fhucho.android_utils;

import java.util.Random;

public class Random128BitId {
	private static final char[] hexArray = "0123456789abcdef".toCharArray();
	private static final Random random = new Random();

	public static String generate() {
		byte[] bytes = new byte[16];
		random.nextBytes(bytes);		
		return bytesToHex(bytes);
	}
	
	private static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		
		return new String(hexChars);
	}
}

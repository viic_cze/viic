package cz.fhucho.android_utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

public class Ui {
	public static void bindClick(Activity activity, int id, Object object, String methodName) {
		ClickBinder.bindClick(findActivityContentView(activity), id, object, methodName);
	}

	public static void bindClick(View view, int id, Object object, String methodName) {
		ClickBinder.bindClick(view, id, object, methodName);
	}
	
	public static void bindClick(View view, Object object, String methodName) {
		ClickBinder.bindClick(view, object, methodName);
	}

	public static ImageView imageView(View view, int id) {
		return view(view, id);
	}

	public static ImageView imageView(Activity activity, int id) {
		return view(activity, id);
	}

	public static LinearLayout linearLayout(View view, int id) {
		return view(view, id);
	}

	public static LinearLayout linearLayout(Activity activity, int id) {
		return view(activity, id);
	}

	public static ListView listView(Activity activity, int id) {
		return view(activity, id);
	}

	public static ListView listView(View view, int id) {
		return view(view, id);
	}

	public static ScrollView scrollView(Activity activity, int id) {
		return view(activity, id);
	}

	public static ScrollView scrollView(View view, int id) {
		return view(view, id);
	}

	public static TextView textView(Activity activity, int id) {
		return view(activity, id);
	}

	public static TextView textView(View view, int id) {
		return view(view, id);
	}

	public static ViewGroup viewGroup(Activity activity, int id) {
		return view(activity, id);
	}

	public static ViewGroup viewGroup(View view, int id) {
		return view(view, id);
	}

	@SuppressWarnings("unchecked")
	public static <T extends View> T view(Activity activity, int id) {
		return (T) activity.findViewById(id);
	}

	@SuppressWarnings("unchecked")
	public static <T extends View> T view(View view, int id) {
		return (T) view.findViewById(id);
	}

	private static View findActivityContentView(Activity activity) {
		return activity.findViewById(android.R.id.content);
	}

	private final View view;

	public Ui(Activity activity) {
		this.view = findActivityContentView(activity);
	}

	public Ui(View view) {
		this.view = view;
	}

	public void bindClick(int id, Object object, String methodName) {
		ClickBinder.bindClick(view, id, object, methodName);
	}

	public ImageView imageView(int id) {
		return view(id);
	}

	public LinearLayout linearLayout(int id) {
		return view(id);
	}

	public ListView listView(int id) {
		return view(id);
	}

	public ScrollView scrollView(int id) {
		return view(id);
	}

	public TextView textView(int id) {
		return view(id);
	}

	public ViewGroup viewGroup(int id) {
		return view(id);
	}

	@SuppressWarnings("unchecked")
	public <T extends View> T view(int id) {
		return (T) view.findViewById(id);
	}

	private static class ClickBinder {
		private static void bindClick(View view, final Object target, String methodName) {
			final Method method = getAndCheckMethod(target, methodName);
			
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					ClickBinder.onClick(target, method, v);
				}
			});
		}
		
		private static void bindClick(View view, int id, final Object target, String methodName) {
			View bindedView = view.findViewById(id);
			if (bindedView == null) {
				throw new RuntimeException("View with the supplied id not found");
			}

			ClickBinder.bindClick(bindedView, target, methodName);
		}

		private static String formatExpectedMethod(Object object, String methodName) {
			return object.getClass().getCanonicalName() + "." + methodName + "(View view)";
		}

		private static Method getAndCheckMethod(Object object, String methodName) {
			Method method;
			try {
				method = object.getClass().getMethod(methodName, View.class);
			} catch (NoSuchMethodException e) {
				String message = "Method " + formatExpectedMethod(object, methodName) + " not found";
				throw new RuntimeException(message, e);
			}

			if (method.getExceptionTypes().length > 0) {
				throw new RuntimeException("The method " + formatExpectedMethod(object, methodName)
						+ " mustn't declare any exceptions");
			}

			return method;
		}

		private static void onClick(Object target, Method method, View view) {
			try {
				method.invoke(target, view);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			} catch (InvocationTargetException e) {
				if (e.getCause() instanceof RuntimeException) {
					throw (RuntimeException) e.getCause();
				} else {
					throw new AssertionError();
				}
			}
		}
	}

}
